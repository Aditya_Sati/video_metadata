<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Video;
use App\User;

class UserController extends Controller {

    //

    /*
     * function is used to return total video size
     * @param none 
     * @return response json
     */
    public function videoSize(Request $request) {
        try {
            $validator = Validator::make($request->all(), [
                        'name' => 'required|string|exists:users'
            ]);
            if ($validator->fails()) {
                return \Illuminate\Support\Facades\Response::json($validator->messages()->first(), Response::HTTP_BAD_REQUEST)->header('Content-Type', "application/json");
            }
            $user = User::where('name', $request->name)->first();
            $videos = Video::where('user_id', $user['id'])->sum('size');
            return \Illuminate\Support\Facades\Response::json($videos, Response::HTTP_OK)->header('Content-Type', "application/json");
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

    /*
     * function is used to return video meta data
     * @param none 
     * @return response json
     */

    public function videoData($id) {
        try {
            $videos = Video::where('id', $id)->first();
            return \Illuminate\Support\Facades\Response::json($videos, Response::HTTP_OK)->header('Content-Type', "application/json");
        } catch (Exception $e) {
            report($e);
            return false;
        }
    }

    /*
     * function is used to return total video size
     * @param none 
     * @return response json
     */

    public function update(Request $request, $id) {
        try {
            $validator = Validator::make($request->all(), [
                        'size' => 'required',
                        'viewers' => 'required'
            ]);
            if ($validator->fails()) {
                return \Illuminate\Support\Facades\Response::json($validator->messages()->first(), Response::HTTP_BAD_REQUEST)->header('Content-Type', "application/json");
            }
            $video = Video::where('id', $id)->first();
            $video->size = $request->size;
            $video->viewers = $request->viewers;
            $video->save();
            return \Illuminate\Support\Facades\Response::json("Data updated successfully", Response::HTTP_OK)->header('Content-Type', "application/json");
        } catch (Exception $e) {
            report($e);

            return false;
        }
    }

}
